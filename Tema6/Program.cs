﻿using System;
namespace Tema6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Matematica.CalculeazaModul(3));
            Console.WriteLine(Matematica.CalculeazaModul(4.5));
            Console.WriteLine(Matematica.CalculeazaSuma(4, 5));
            Console.WriteLine(Matematica.CalculeazaSuma(2, 3 , 4));

            Animal animal1 = new Pisica();
            Animal animal2 = new Caine();
            Animal animal3 = new Vaca();
            animal1.ScoateSunet();
            animal2.ScoateSunet();
            animal3.ScoateSunet();

            //Conversie de tipuri

            short shortVar = 31;
            int intVar = 54;
            decimal decimalVar = 123.33m;

            Convert.ToBoolean(shortVar);
            Convert.ToByte(intVar);
            Convert.ToDateTime(decimalVar);

        }
    }
}
