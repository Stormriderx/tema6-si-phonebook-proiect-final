﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema6
{
   public static class Matematica
   {
        public static int CalculeazaModul(int numar)
        {
            if (numar < 0)
                return numar;
            else
                return numar;
        }
        public static double CalculeazaModul(double numar)
        {
            if (numar < 0)
                return numar;
            else
                return numar;
        }
        public static int CalculeazaSuma(int x, int y)
        {
            return x + y;
        }
        public static int CalculeazaSuma(int x, int y, int z)
        {
            return x + y + z;
        }
   }
}
